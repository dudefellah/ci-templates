# Gitlab CI/CD Temlates

This is a public repository for Gitlab CI/CD templates that I find useful
which I haven't seen covered elsewhere. All templates are categorized by
subdirectory, and each subdirectory should have its own README.md which
describes the usage and available values for the templates under that path.

All jobs are created with the intention to be included with the
[https://docs.gitlab.com/ee/ci/yaml/#extends](extends) keyword due to the
flexibility and reusabiliy allowed with this approach.

# Author(s)

* Dan Thomson - https://gitlab.com/dudefellah
