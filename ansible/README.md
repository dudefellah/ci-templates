# Ansible Templates

This directory contains Gitlab CI/CD jobs that you can use in your pipeline
for testing and publishing your Ansible content.

## Ansible Galaxy Publish

Example Usage:

```yaml
include:
  - remote: "https://gitlab.com/dudefellah/ci-templates/-/raw/master/ansible/Galaxy.gitlab-ci.yml"

galaxy_publish:
  extends: .ansible_galaxy_publish
```

### Values

The values that can be used by this job are listed below. To ensure that you
have a complete up to date view of the variables, you should double check
by checking the actual
[ansible/Galaxy.gitlab-ci.yml](Galaxy.gitlab-ci.yml file) in case some things
were missed in the documentation.

|Name|Description|Default|
|----|-----------|-------|
|`ANSIBLE_GALAXY_GITHUB_REPO`| The name of the Github repo to be used for importing by Ansible Galaxy. If none is specified, the current Gitlab project will be used, but with the host (likely `gitlab.com`) replaced with `github.com`. This would mean that a project called `gitlab.com/mystuff/myproject` will turn into `github.com/mystuff/myproject`. | `""` |
|`ANSIBLE_GALAXY_GITHUB_USER`| The Github username to use when connecting to Ansible Galaxy with your API token. If nothing is specified, the namespace of this projet will be used. | `$CI_PROJECT_NAMESPACE` |
|`ANSIBLE_GALAXY_BRANCH` | The branch that will be deployed into Ansible Galaxy | `$CI_COMMIT_REF_NAME` |
|`ANSIBLE_GALAXY_API_SERVER` | The Ansible Galaxy API server to deploy to | `"https://galaxy.ansible.com/api/"` |
|`ANSIBLE_GALAXY_API_KEY` | The API key to use for authenticating to Ansible Galaxy. The job will fail if this value is unset. | `""` |

## Ansible Molecule Testing

Example Usage:

```yaml
include:
  - remote: "https://gitlab.com/dudefellah/ci-templates/-/raw/master/ansible/Molecule.gitlab-ci.yml"

molecule:
  extends: .ansible_molecule
```
### Values

The values that can be used by this job are listed below. To ensure that you
have a complete up to date view of the variables, you should double check
by checking the actual
[ansible/Molecule.gitlab-ci.yml](Molecule.gitlab-ci.yml file) in case some things
were missed in the documentation.

|Name|Description|Default|
|----|-----------|-------|
|`ANSIBLE_MOLECULE_PIP_REQUIREMENTS` | Additional pip requirements for your role can be added to the file pointed to by `$ANSIBLE_MOLECULE_PIPE_REQUIREMENTS` and they will be installed | `"requirements.txt"` |
|`ANSIBLE_MOLECULE_PIP_MODULES` | Custom pip modules that you want to add during the CI/CD process but don't necessarily want to include in a `requirements.txt` file can be added here. | `""` |
|`ANSIBLE_MOLECULE_GALAXY_REQUIREMENTS` | Requirements file with Galaxy role and collections required to perform the molecule test(s). | `"meta/requirements.yml"` |
|`ANSIBLE_MOLECULE_SCENARIOS` | A whitespace separated list of scenarios to run with Molecule. By default all tests are run. | `""` |
